package com.dev.sendmail.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
public class ThymeleafTemplateConfig {
    @Bean
    public ITemplateResolver templateResolver() {
        ClassLoaderTemplateResolver template = new ClassLoaderTemplateResolver();
        template.setPrefix("templates/");
        template.setSuffix(".html");
        template.setTemplateMode("HTML");
        template.setCharacterEncoding("UTF-8");

        return template;
    }

    @Bean
    public SpringTemplateEngine springTemplateEngine(ITemplateResolver templateResolver) {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        return templateEngine;
    }
}
