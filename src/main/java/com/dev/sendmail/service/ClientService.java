package com.dev.sendmail.service;

import com.dev.sendmail.dto.sdi.ClientSdi;

import javax.mail.MessagingException;

public interface ClientService {
    Boolean create(ClientSdi sdi);
}
