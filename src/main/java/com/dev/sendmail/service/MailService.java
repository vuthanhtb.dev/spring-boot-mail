package com.dev.sendmail.service;

import com.dev.sendmail.dto.DataMailDTO;

import javax.mail.MessagingException;

public interface MailService {
    void sendMail(DataMailDTO dataMail, String templateName) throws MessagingException;
}
